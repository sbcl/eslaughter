.\" -*- Mode: Text -*-
.\"
.\" man page introduction to SBCL
.\"
.\" SBCL, including this man page, is derived from CMU Common Lisp, of
.\" which it was said (ca. 1991)
.\"   **********************************************************************
.\"   This code was written as part of the CMU Common Lisp project at
.\"   Carnegie Mellon University, and has been placed in the public domain.
.\"   If you want to use this code or any part of CMU Common Lisp, please
.\"   contact Scott Fahlman or slisp-group@cs.cmu.edu.
.\"   **********************************************************************
.\" Most of SBCL, including this man page, is in the public domain. See
.\" COPYING in the distribution for more information.
.\"
.TH SBCL 1 "$Date$"
.AT 3
.SH NAME
SBCL -- Steel Bank Common Lisp

.SH DESCRIPTION

SBCL is a free Common Lisp programming environment. It is derived from
the free CMU CL programming environment. (The name is intended to
acknowledge the connection: steel and banking are the industries where
Carnegie and Mellon made the big bucks.)

.SH LICENSING

It is free software, mostly in the public domain, but with some
subsystems under BSD-style licenses which allow modification and
reuse as long as credit is given. It is provided "as is", with no
warranty of any kind.

For more information about license issues, see the COPYING file in
the distribution. For more information about history, see the 
CREDITS file in the distribution.

.SH RUNNING SBCL

To run SBCL, type "sbcl" at the command line with no arguments. (SBCL
understands command line arguments, but you probably won't need to use
them unless you're a fairly advanced user. If you are, you should read
the COMMAND LINE SYNTAX section, below.) You should see some startup
messages, then a prompt ("\f(CR*\fR").  Type a Lisp expression at the prompt,
and SBCL will read it, execute it, print any values returned, give you
another prompt, and wait for your next input.  For example,
\f(CR
  * (+ 1 2 3)

  6
  * (funcall (lambda (x y) (list x y y)) :toy :choo)

  (:TOY :CHOO :CHOO)
  * "Hello World"

  "Hello World"
  *
\fR

Many people like to run SBCL, like other Lisp systems, as a subprocess
under Emacs. The Emacs "Slime" and "ilisp" modes provide many
convenient features, like command line editing, tab completion, and
various kinds of coupling between Common Lisp source files and the
interactive SBCL subprocess, but they can be somewhat fragile with
respect to packages and readtables, in which case SBCL in the Emacs
"shell" mode can be a useful substitute.

.SH OVERVIEW

SBCL compiles Common Lisp to native code. (Even today, some 30 years
after the MacLisp compiler, people will tell you that Lisp is an
interpreted language. Ignore them.)

SBCL aims for but has not completely achieved compliance with the ANSI
standard for Common Lisp. More information about this is available in
the BUGS section below.

SBCL also includes various non-ANSI extensions, described more fully
in the User Manual.  Some of these are in the base system and others
are "contrib" modules loaded on request using \f(CRREQUIRE\fR.  For
example, to load the \f(CRSB\-BSD\-SOCKETS\fR module that provides
TCP/IP connectivity,
\f(CR
   * (require \(aqasdf)
   * (require \(aqsb\-bsd\-sockets)
\fR

Many Lispy extensions have been retained from CMU CL:
.TP 3
\--
CMU-CL-style safe implementation of type declarations:
"Declarations are assertions."
.TP 3
\--
the source level debugger (very similar to CMU CL's)
.TP 3
\--
the profiler (now somewhat different from CMU CL's)
.TP 3
\--
saving the state of the running SBCL process, producing a
"core" file which can be restarted later
.TP 3
\--
Gray streams (a de-facto standard system of overloadable CLOS classes
whose instances can be used wherever ordinary ANSI streams can be used)
.TP 3
\--
weak pointers and finalization
.PP

Fundamental system interface extensions are also provided:
.TP 3
\--
calling out to C code (a.k.a. FFI, foreign function interface,
with very nearly the same interface as CMU CL)
.TP 3
\--
some simple support for operations with a "scripting language" flavor,
\fIe.g.\fR reading POSIX \f(CRargc\fR and \f(CRargv\fR, or executing a
subprogram
.PP

.SH DIFFERENCES FROM CMU CL

SBCL can be built from scratch using a plain vanilla ANSI Common Lisp
system and a C compiler, and all of its properties are specified by
the version of the source code that it was created from. This clean
bootstrappability was the immediate motivation for forking off of the
CMU CL development tree. A variety of implementation differences are
motivated by this design goal.

Maintenance work in SBCL since the fork has diverged somewhat from the
maintenance work in CMU CL. Many but not all bug fixes and
improvements have been shared between the two projects, and sometimes
the two projects disagree about what would be an improvement.

Most extensions supported by CMU CL have been unbundled from SBCL,
including Motif support, the Hemlock editor, search paths, the
low-level Unix interface, the WIRE protocol, various user-level macros
and functions (\fIe.g.\fR \f(CRLETF\fR, \f(CRITERATE\fR, \f(CRMEMQ\fR,
\f(CRREQUIRED\-ARGUMENT\fR), and many others.

SBCL implements multithreading, but in a completely different fashion
from CMU CL: see the User Manual for details. As of 1.0.13 this is
still considered beta-quality and must be explicitly enabled at build
time.

SBCL has retained some extensions from its parent CMU CL. Many of the
retained extensions are in these categories:
.TP 3
\--
things which might be in the new ANSI spec, \fIe.g.\fR safe type
declarations, weak pointers, finalization, foreign function
interface to C, and Gray streams;
.TP 3
\--
things which are universally available in Unix scripting languages,
\fIe.g.\fR \f(CRRUN\-PROGRAM\fR and POSIX \f(CRargv\fR and \f(CRgetenv\fR;
.TP 3
\--
hooks into the low level workings of the system which can be useful
for debugging, \fIe.g.\fR requesting that a particular function be executed
whenever GC occurs, or tuning compiler diagnostic output;
.TP 3
\--
unportable performance hacks, \fIe.g.\fR \f(CRFREEZE\-TYPE\fR and
\f(CRPURIFY\fR. For more information about these, look at the online
documentation for symbols in the \f(CRSB\-EXT\fR package, and look at the user
manual.
.PP

There are also a few retained extensions which don't fall into any
particular category, \fIe.g.\fR the ability to save running Lisp images as
executable files.

Some of the retained extensions have new names and/or different
options than their CMU CL counterparts. For example, the SBCL function
which saves a Lisp image to disk and kills the running process is
called \f(CRSAVE\-LISP\-AND\-DIE\fR instead of \f(CRSAVE\-LISP\fR, and
SBCL's \f(CRSAVE\-LISP\-AND\-DIE\fR supports fewer keyword options
than CMU CL's \f(CRSAVE\-LISP\fR does.

(Why doesn't SBCL support more extensions natively?  Why drop all
those nice extensions from CMU CL when the code already exists? This
is a frequently asked question on the mailing list.  There are two
principal reasons.  First, it's a design philosophy issue: arguably
SBCL has done its job by supplying a stable FFI, and the right design
decision is to move functionality derived from that, like socket
support, into separate libraries.  Some of these are distributed with
SBCL as "contrib" modules, others are distributed as separate software
packages by separate maintainers. Second, it's a practical decision -
focusing on a smaller number of things will, we hope, let us do a
better job on them.)

.SH THE COMPILER

SBCL inherits from CMU CL the "Python" native code compiler. (Though
we often avoid that name in order to avoid confusion with the
scripting language also called Python.) This compiler is very clever
about understanding the type system of Common Lisp and using it to
optimize code, and about producing notes to let the user know when the
compiler doesn't have enough type information to produce efficient
code. It also tries (almost always successfully) to follow the unusual
but very useful principle that "declarations are assertions", \fIi.e.\fR
type declarations should be checked at runtime unless the user
explicitly tells the system that speed is more important than safety.

The compiled code uses garbage collection to automatically
manage memory. The garbage collector implementation varies considerably
from CPU to CPU. In particular, on some CPUs the GC is nearly exact,
while on others it's more conservative, and on some CPUs the GC
is generational, while on others simpler stop and copy strategies
are used.

For more information about the compiler, see the user manual.

.SH COMMAND LINE SYNTAX

Command line syntax can be considered an advanced topic; for ordinary
interactive use, no command line arguments should be necessary.

In order to understand the command line argument syntax for SBCL, it
is helpful to understand that the SBCL system is implemented as two
components, a low-level runtime environment written in C and a
higher-level system written in Common Lisp itself. Some command line
arguments are processed during the initialization of the low-level
runtime environment, some command line arguments are processed during
the initialization of the Common Lisp system, and any remaining
command line arguments are passed on to user code.

The full, unambiguous syntax for invoking SBCL at the command line is
.TP 3
.B sbcl [runtime options] \-\-end\-runtime\-options [toplevel options] \-\-end\-toplevel\-options [user options]
.PP

For convenience, the \-\-end\-runtime\-options and \-\-end\-toplevel\-options
elements can be omitted. Omitting these elements can be convenient
when you are running the program interactively, and you can see that
no ambiguities are possible with the option values you are using.
Omitting these elements is probably a bad idea for any batch file
where any of the options are under user control, since it makes it
impossible for SBCL to detect erroneous command line input, so that
erroneous command line arguments will be passed on to the user program
even if they was intended for the runtime system or the Lisp system.

Supported runtime options are
.TP 3
.B \-\-core <corefilename>
Run the specified Lisp core file instead of the default. (See the FILES
section for the standard core, or the system documentation for
\f(CRSB\-EXT:SAVE\-LISP\-AND\-DIE\fR for information about how to create a 
custom core.) Note that if the Lisp core file is a user-created core
file, it may run a nonstandard toplevel which does not recognize the
standard toplevel options.
.TP 3
.B \-\-dynamic-space-size <megabytes>
Size of the dynamic space reserved on startup in megabytes. Default value
is platform dependent.
.TP 3
.B \-\-control-stack-size <megabytes>
Size of control stack reserved for each thread in megabytes. Default value
is 2.
.TP 3
.B \-\-noinform
Suppress the printing of any banner or other informational message at
startup. (This makes it easier to write Lisp programs which work
cleanly in Unix pipelines. See also the "\-\-noprint" and
"\-\-disable\-debugger" options.)
.TP 3
.B \-\-help
Print some basic information about SBCL, then exit.
.TP 3
.B \-\-version
Print SBCL's version information, then exit.
.PP

In the future, runtime options may be added to control behavior such
as lazy allocation of memory.

Runtime options, including any \-\-end\-runtime\-options option,
are stripped out of the command line before the
Lisp toplevel logic gets a chance to see it.

The toplevel options supported by the standard SBCL core are
.TP 3
.B \-\-sysinit <filename>
Load filename instead of the default system-wide initialization file.
(See the FILES section.)
.TP 3
.B \-\-no\-sysinit
Do not load a system-wide initialization file. If this option is
given, the \-\-sysinit option is ignored.
.TP 3
.B \-\-userinit <filename>
Load filename instead of the default user initialization file. (See
the FILES section.)
.TP 3
.B \-\-no\-userinit
Do not load a user initialization file. If this option is
given, the \-\-userinit option is ignored.
.TP 3
.B \-\-eval <command>
After executing any initialization file, but before starting the
read-eval-print loop on standard input, read and evaluate the command
given. More than one \-\-eval option can be used, and all will be read
and executed, in the order they appear on the command line.
.TP 3
.B \-\-load <filename>
This is equivalent to \-\-eval \(aq(load "<filename>")\(aq. The special
syntax is intended to reduce quoting headaches when invoking SBCL
from shell scripts.
.TP 3
.B \-\-noprint
When ordinarily the toplevel "read-eval-print loop" would be executed,
execute a "read-eval loop" instead, \fIi.e.\fR don't print a prompt and
don't echo results. Combined with the \-\-noinform runtime option, this
makes it easier to write Lisp "scripts" which work cleanly in Unix
pipelines.
.TP 3
.B \-\-disable\-debugger
This is equivalent to \-\-eval \(aq(sb\-ext:disable\-debugger)\(aq. By
default, a Common Lisp system tries to ask the programmer for help
when it gets in trouble (by printing a debug prompt, then listening,
on \f(CR*DEBUG\-IO*\fR). However, this is not useful behavior for a system
running with no programmer available, and this option tries to set up
more appropriate behavior for that situation. This is implemented by
redefining \f(CRINVOKE\-DEBUGGER\fR so that any call exits the process with a
failure code after printing a backtrace. (Note that because it is
implemented by modifying special variables and \f(CRFDEFINITION\fRs, its
effects persist in .core files created by
\f(CRSB\-EXT:SAVE\-LISP\-AND\-DIE\fR.  If you want to undo its
effects, \fIe.g.\fR if you build a system unattended and then want to
operate a derived system interactively, see the
\f(CRSB\-EXT:ENABLE\-DEBUGGER\fR command.)
.PP

Regardless of the order in which \-\-sysinit, \-\-userinit, and
\-\-eval options appear on the command line, the sysinit file, if it
exists, is loaded first; then the userinit file, if it exists, is
loaded; then any \-\-eval commands are read and executed in sequence;
then the read-eval-print loop is started on standard input. At any
step, error conditions or commands such as \f(CRSB\-EXT:QUIT\fR can
cause execution to be terminated before proceeding to subsequent
steps.

Note that when running SBCL with the \-\-core option, using a core
file created by a user call to the
\f(CRSB\-EXT:SAVE\-LISP\-AND\-DIE\fR, the toplevel options may be
under the control of user code passed as arguments to
\f(CRSB\-EXT:SAVE\-LISP\-AND\-DIE\fR. For this purpose, the
\-\-end\-toplevel\-options option itself can be considered a toplevel
option, \fIi.e.\fR the user core, at its option, may not support it.

In the standard SBCL startup sequence (\fIi.e.\fR with no user core
involved) toplevel options and any \-\-end\-toplevel\-options option are
stripped out of the command line argument list before user code gets a
chance to see it.

.SH SYSTEM REQUIREMENTS

SBCL currently runs on X86 (Linux, FreeBSD, OpenBSD, and NetBSD),
X86-64 (Linux), Alpha (Linux, Tru64), PPC (Linux, Darwin/MacOS X),
SPARC (Linux and Solaris 2.x), and MIPS (Linux).  For information on
other ongoing and possible ports, see the sbcl\-devel mailing list,
and/or the web site.

SBCL requires on the order of 16Mb RAM to run on X86 systems, though
all but the smallest programs would be happier with 32Mb or more.

.SH KNOWN BUGS

This section attempts to list the most serious and long-standing bugs.
For more detailed and current information on bugs, see the BUGS file
in the distribution.

It is possible to get in deep trouble by exhausting heap memory.  The
SBCL system overcommits memory at startup, so, on typical Unix-alikes
like Linux and FreeBSD, this means that if the SBCL system turns out
to use more virtual memory than the system has available for it, other
processes tend to be killed randomly (!).

The compiler's handling of function return values unnecessarily
violates the "declarations are assertions" principle that it otherwise
adheres to. Using \f(CRPROCLAIM\fR or \f(CRDECLAIM\fR to specify the
return type of a function causes the compiler to believe you without
checking. Thus compiling a file containing
\f(CR
  (DECLAIM (FTYPE (FUNCTION (T) NULL) SOMETIMES))
  (DEFUN SOMETIMES (X) (ODDP X))
  (DEFUN FOO (X) (IF (SOMETIMES X) \(aqTHIS\-TIME \(aqNOT\-THIS\-TIME))\fR
.br
then running \f(CR(FOO 1)\fR gives \f(CRNOT\-THIS\-TIME\fR, because
the compiler relied on the truth of the \f(CRDECLAIM\fR without checking it.

Some things are implemented very inefficiently.
.TP 3
\--
Multidimensional arrays are inefficient, especially
multidimensional arrays of floating point numbers.
.TP 3
\--
CLOS isn't particularly efficient. (In part, CLOS is so dynamic
that it's slow for fundamental reasons, but beyond that, the
SBCL implementation of CLOS doesn't do some important known
optimizations.)
.TP 3
\--
SBCL, like most (maybe all?) implementations of Common Lisp on stock
hardware, has trouble passing floating point numbers around
efficiently, because a floating point number, plus a few extra bits to
identify its type, is larger than a machine word. (Thus, they get
"boxed" in heap-allocated storage, causing GC overhead.) Within a
single compilation unit, or when doing built-in operations like
\f(CRSQRT\fR and \f(CRAREF\fR, or some special operations like
structure slot accesses, this is avoidable: see the user manual for
some efficiency hints. But for general function calls across the
boundaries of compilation units, passing the result of a floating
point calculation as a function argument (or returning a floating
point result as a function value) is a fundamentally slow operation.
.PP

.SH REPORTING BUGS

To report a bug, please send mail to the mailing lists sbcl-help or
sbcl-devel. You can find the complete mailing list addresses on the
web pages at <\f(CRhttp://sbcl.sourceforge.net/\fR>; note that as a
spam reduction measure you must subscribe to the lists before you can
post. (You may also find fancy SourceForge bug-tracking machinery
there, but don't be fooled. As of 2002-07-25 anyway, we don't actively
monitor that machinery, and it exists only because we haven't been
able to figure out how to turn it off.)

As with any software bug report, it's most helpful if you can provide
enough information to reproduce the symptoms reliably, and if you say
clearly what the symptoms are.  For example, "There seems to be
something wrong with TAN of very small negative arguments. When I
execute \f(CR(TAN LEAST\-NEGATIVE\-SINGLE\-FLOAT)\fR interactively on
sbcl-1.2.3 on my Linux 4.5 X86 box, I get an \f(CRUNBOUND\-VARIABLE\fR
error."

.SH SUPPORT

Various information about SBCL is available at
<\f(CRhttp://www.sbcl.org/\fR>. The mailing lists there are the recommended
place to look for support.

.SH ENVIRONMENT

.TP 10n
.BR SBCL_HOME
This variable controls where files like "sbclrc", "sbcl.core", and the
add-on "contrib" systems are searched for.  If it is not set, then
sbcl sets it from a compile-time default location which is usually
/usr/local/lib/sbcl/ but may have been changed \fIe.g.\fR by a third-party
packager.

.SH FILES

.TP
.I sbcl
executable program containing some low-level runtime support and
a loader, used to read sbcl.core
.TP
.I sbcl.core
dumped memory image containing most of SBCL, to be loaded by
the `sbcl' executable.  Looked for in $\f(CRSBCL_HOME\fR,
unless overridden by the \f(CR\-\-core\fR option.
.TP
.I sbclrc
optional system-wide startup script, looked for in $\f(CRSBCL_HOME\fR/sbclrc
then /etc/sbclrc, unless overridden by the \f(CR\-\-sysinit\fR command line
option.
.TP
.I .sbclrc
optional per-user customizable startup script (in user's home
directory, or as specified by  \f(CR\-\-userinit\fR)

.SH AUTHORS

Dozens of people have made substantial contributions to SBCL and its
subsystems, and to the CMU CL system on which it was based, over the
years. See the CREDITS file in the distribution for more information.

.SH SEE ALSO

Full SBCL documentation is maintained as a Texinfo manual. If is has
been installed, the command
.IP
.B info sbcl
.PP
should give you access to the complete manual. Depending on your
installation it may also be available in HTML and PDF formats in eg.
.IP
.B /usr/local/share/doc/sbcl/
.PP
See the SBCL homepage 
.IP
.B <\f(CRhttp://www.sbcl.org/\fR>
.PP
for more information, including directions on how to subscribe to the
sbcl\-devel and sbcl\-help mailing-lists.
